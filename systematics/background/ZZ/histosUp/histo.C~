#include "TH1D.h"
#include "TFile.h"
#include "TFractionFitter.h"
#include <iostream>
using namespace std;

void histo(){

  TFile * fileTrans = TFile::Open("/afs/cern.ch/work/l/llorente/HWW/HWWlvlv2015/share/results-v17b-wz0/results-alpgen/run2-ZjetsFakeFactor-noWeight-e-nominal/plots/ll-CutWZOptimHigh-transMassW-log.root");

  const int nBins = 17;
  Double_t xBins[nBins+1] = {0.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0, 120.0, 140.0, 160.0, 190.0, 220.0, 250.0, 300.0};

  TH1D * h0_data = (TH1D*) fileTrans->Get("hist_data"); TH1D * h1_data = (TH1D*) h0_data->Rebin(nBins, "h1_data", xBins);

  //WZ histogram
  TH1D * h0_wz = (TH1D*) fileTrans->Get("hist_WZ"); TH1D * h1_wz = (TH1D*) h0_wz->Rebin(nBins, "h1_wz", xBins);

  //Other backgrounds
  vector<TH1D*> vecBkg; vecBkg.clear();
  TH1D * h0_zj = (TH1D*) fileTrans->Get("hist_Zjets"); TH1D * h1_zj = (TH1D*) h0_zj->Rebin(nBins, "h1_zj", xBins); vecBkg.push_back(h1_zj);
  TH1D * h0_vg = (TH1D*) fileTrans->Get("hist_Vgamma"); TH1D * h1_vg = (TH1D*) h0_vg->Rebin(nBins, "h1_vg", xBins); vecBkg.push_back(h1_vg);
  TH1D * h0_ww = (TH1D*) fileTrans->Get("hist_WW"); TH1D * h1_ww = (TH1D*) h0_ww->Rebin(nBins, "h1_ww", xBins); vecBkg.push_back(h1_ww);
  TH1D * h0_zz = (TH1D*) fileTrans->Get("hist_ZZ"); h0_zz->Scale(1.13);
  TH1D * h1_zz = (TH1D*) h0_zz->Rebin(nBins, "h1_zz", xBins); vecBkg.push_back(h1_zz);
  TH1D * h0_tt = (TH1D*) fileTrans->Get("hist_ttbar"); TH1D * h1_tt = (TH1D*) h0_tt->Rebin(nBins, "h1_tt", xBins); vecBkg.push_back(h1_tt);
  TH1D * h0_st = (TH1D*) fileTrans->Get("hist_single_top"); TH1D * h1_st = (TH1D*) h0_st->Rebin(nBins, "h1_st", xBins); vecBkg.push_back(h1_st);
  TH1D * h0_ttW = (TH1D*) fileTrans->Get("hist_ttW"); TH1D * h1_ttW = (TH1D*) h0_ttW->Rebin(nBins, "h1_ttW", xBins); vecBkg.push_back(h1_ttW);
  TH1D * h0_ttZ = (TH1D*) fileTrans->Get("hist_ttZ"); TH1D * h1_ttZ = (TH1D*) h0_ttZ->Rebin(nBins, "h1_ttZ", xBins); vecBkg.push_back(h1_ttZ);
  TH1D * h0_VVV = (TH1D*) fileTrans->Get("hist_VVV"); TH1D * h1_VVV = (TH1D*) h0_VVV->Rebin(nBins, "h1_VVV", xBins); vecBkg.push_back(h1_VVV);

  //Normalize to the bin width
  for (int k = 0; k < h1_data->GetNbinsX(); ++k){
    double binWidth = (xBins[k+1]-xBins[k])/10;
    h1_data->SetBinContent(k+1, h1_data->GetBinContent(k+1)/binWidth);
    h1_data->SetBinError(k+1, h1_data->GetBinError(k+1)/binWidth);

    h1_wz->SetBinContent(k+1, h1_wz->GetBinContent(k+1)/binWidth);
    h1_wz->SetBinError(k+1, h1_wz->GetBinError(k+1)/binWidth);

    for (int iBkg = 0; iBkg < vecBkg.size(); ++iBkg){
      vecBkg[iBkg]->SetBinContent(k+1, vecBkg[iBkg]->GetBinContent(k+1)/binWidth);
      vecBkg[iBkg]->SetBinError(k+1, vecBkg[iBkg]->GetBinError(k+1)/binWidth);
    }
  }

  //Background histogram
  TH1D * h_bkg = new TH1D("h_bkg","h_bkg",nBins, xBins);
  TH1D * h_data = new TH1D("h_data","h_data",nBins, xBins);
  TH1D * h_wz = new TH1D("h_wz","h_wz",nBins, xBins);

  for (int k = 5; k < h_bkg->GetNbinsX(); ++k){

    double sumBkg = 0.0; double errBkg2 = 0.0;
    for (int iBkg = 0; iBkg < vecBkg.size(); ++iBkg){
      sumBkg += vecBkg[iBkg]->GetBinContent(k+1);
      errBkg2 += pow(vecBkg[iBkg]->GetBinError(k+1),2);
    }

    h_bkg->SetBinContent(k+1, sumBkg);
    h_bkg->SetBinError(k+1, sqrt(errBkg2));

    h_data->SetBinContent(k+1, h1_data->GetBinContent(k+1));
    h_data->SetBinError(k+1, h1_data->GetBinError(k+1));

    h_wz->SetBinContent(k+1, h1_wz->GetBinContent(k+1));
    h_wz->SetBinError(k+1, h1_wz->GetBinError(k+1));
  }


  TFile * fileHistos = new TFile("histos.root","RECREATE");
  fileHistos->cd();
  h_wz->SetNameTitle("histoWZ","histoWZ"); h_wz->Write();
  h_bkg->SetNameTitle("histoBkg","histoBkg"); h_bkg->Write();
  h_data->SetNameTitle("histoData","histoData"); h_data->Write();
  fileHistos->Close();



}
