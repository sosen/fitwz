#include "fcn.C"
#include "TH1D.h"
#include "TMinuit.h"
#include "TLine.h"

void chi2Min(){

  static const int nPar = 1;

  TMinuit minuit(nPar);
  minuit.SetFCN(fcn);

  double par[nPar];
  double stepSize[nPar];
  double minVal[nPar];
  double maxVal[nPar];
  string parName[nPar];

  par[0]= 1.0;
  stepSize[0]=0.0000001;
  minVal[0]=0.0;
  maxVal[0]=2.0;
  parName[0]=" alpha ";

  minuit.SetErrorDef(0);
  minuit.SetMaxIterations(100000000000000000);
  minuit.DefineParameter(0,parName[0].c_str(),par[0],stepSize[0],minVal[0],maxVal[0]);

  minuit.Migrad();
  double outpar[nPar],errpar[nPar];
  minuit.GetParameter(0,outpar[0],errpar[0]);

  double alphaValue = outpar[0];
  double alphaError = errpar[0];

  double chi2 = minuit.fAmin;


  cout << "===============" << endl;
  cout << "alpha = " << alphaValue << endl;
  cout << "error = " << alphaError << endl;

  cout << "-------------" << endl;
  cout << "chi2 = " << chi2 << endl;


}
