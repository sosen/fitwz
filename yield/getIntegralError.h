#include "TH1D.h"

double getIntegralError(TH1D* histo){

  double err2 = 0.0;
  for (int k = 0; k < histo->GetNbinsX(); ++k){
    err2 += pow(histo->GetBinError(k+1),2);
  }

  return sqrt(err2);
}
