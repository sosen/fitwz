#include "TMinuit.h"
#include "TH1D.h"
#include "TFile.h"
#include "TMath.h"
#include <vector> 
#include <iostream>
using namespace std;

TFile * fileData = TFile::Open("histos/histos.root");

void fcn(int& npar, double* deriv, double& f, double par[], int flag){

  TH1D * histoData = (TH1D*) fileData->Get("histoData");

  TH1D * histoWZ = (TH1D*) fileData->Get("histoWZ");
  TH1D * histoBkg = (TH1D*) fileData->Get("histoBkg");

  double myChi2 = 0;
  for (int k = 5; k < histoData->GetNbinsX(); ++k){

    double data = histoData->GetBinContent(k+1);
    double dataError = histoData->GetBinError(k+1);

    double signal = histoWZ->GetBinContent(k+1);
    double background = histoBkg->GetBinContent(k+1);

    double signalError = histoWZ->GetBinError(k+1);
    double backgroundError = histoBkg->GetBinError(k+1);

    double totalError = sqrt(pow(dataError,2)+pow(signalError,2)+pow(backgroundError,2));

    double theoFunction = par[0]*signal + background;
    myChi2 += pow((data-theoFunction),2)/pow(totalError,2);
  }

  f = myChi2;

  return;
}


